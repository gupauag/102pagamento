package br.com.Pagamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ApplicationPagamento {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationPagamento.class, args);
	}

}
