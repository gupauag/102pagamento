package br.com.Pagamento.Pagamento.client;

import br.com.Pagamento.Pagamento.client.dtoClient.CartaoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("/cartao/{id}")
    Optional<CartaoDTO> consultaCartao(@PathVariable int id);

}
