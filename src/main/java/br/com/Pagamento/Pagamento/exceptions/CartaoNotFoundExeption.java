package br.com.Pagamento.Pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão não encontrado.")
public class CartaoNotFoundExeption extends RuntimeException {
}
