package br.com.Pagamento.Pagamento.exceptions;

import br.com.Pagamento.Pagamento.exceptions.errors.MensagemDeErro;
import br.com.Pagamento.Pagamento.exceptions.errors.ObjetoDeErro;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandler {

    private String mensagemPadrao = "Campos informados incorretamente no Body";

    @ExceptionHandler(FeignException.class)
    public ResponseEntity handleFeignException(FeignException e, HttpServletResponse response)
            throws JSONException, IOException {

        HashMap<String,Object> result =
                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);

        return ResponseEntity.status(e.status()).body(result);
    }

//    @ExceptionHandler(FeignException.BadRequest.class)
//    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
//    public Map<String, Object> handleFeignBadRequestException(FeignException e, HttpServletResponse response)
//            throws JSONException, IOException {
//
//        HashMap<String,Object> result =
//                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);
//
//        return result;
//    }
//    @ExceptionHandler(FeignException.NotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND)
//    public Map<String, Object> handleFeignNotFoundException(FeignException e, HttpServletResponse response)
//            throws JSONException, IOException {
//
//        HashMap<String,Object> result =
//                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);
//
//        return result;
//    }
//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(code = HttpStatus.EXPECTATION_FAILED)
//    public Map<String, Object> teste(FeignException e, HttpServletResponse response)
//            throws JSONException, IOException {
//
//        HashMap<String,Object> result =
//                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);
//
//        return result;
//    }
//
//
//    @ExceptionHandler(MethodArgumentNotValidException.class) // erro disparado na validação dos argumentos passados
//    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY) // informa que não foi processado por conta das entidades
//    @ResponseBody //indica que a resposta será no body da api
//    public MensagemDeErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception) {
//
//        HashMap<String, ObjetoDeErro> erros = new HashMap<>();
//
//        BindingResult resultado = exception.getBindingResult();
//
//        for (FieldError erro : resultado.getFieldErrors()) {
//            erros.put(erro.getField(), new ObjetoDeErro(erro.getDefaultMessage(), erro.getRejectedValue().toString()));
//        }
//
//        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
//                this.mensagemPadrao, erros);
//
//        return mensagemDeErro;
//    }
}
