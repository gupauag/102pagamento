package br.com.Pagamento.Pagamento.models.DTO;

import br.com.Pagamento.Pagamento.models.Pagamento;

public class MappingDTO {

    public PagamentoSaidaDTO mappingPagamentoDTO(Pagamento pagamento){
        PagamentoSaidaDTO saidaDTO = new PagamentoSaidaDTO();
        saidaDTO.setCartao_id(pagamento.getCartaoId());
        saidaDTO.setDescricao(pagamento.getDescricao());
        saidaDTO.setId(pagamento.getId());
        saidaDTO.setValor(pagamento.getValor());
        return saidaDTO;
    }

}
