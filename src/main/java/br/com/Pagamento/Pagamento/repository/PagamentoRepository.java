package br.com.Pagamento.Pagamento.repository;

import br.com.Pagamento.Pagamento.models.Pagamento;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    List<Pagamento> findAllByCartaoId(int idCartao);

    @Modifying
    @Transactional
    @Query(value = "delete from pagamento where cartao_id = :idCartao", nativeQuery = true)
    void deleteCartaoId(@Param("idCartao") int idCartao);
}
