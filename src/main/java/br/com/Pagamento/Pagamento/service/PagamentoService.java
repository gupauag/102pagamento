package br.com.Pagamento.Pagamento.service;

import br.com.Pagamento.Pagamento.client.CartaoClient;
import br.com.Pagamento.Pagamento.client.dtoClient.CartaoDTO;
import br.com.Pagamento.Pagamento.exceptions.CartaoNotActiveException;
import br.com.Pagamento.Pagamento.exceptions.PagamentosNotFoundException;
import br.com.Pagamento.Pagamento.models.DTO.PagamentoEntradaDTO;
import br.com.Pagamento.Pagamento.models.Pagamento;
import br.com.Pagamento.Pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento criarPagamento(PagamentoEntradaDTO pagamentoEntradaDTO){

        Optional<CartaoDTO> optionalCartao = cartaoClient.consultaCartao(pagamentoEntradaDTO.getCartao_id());
        if(optionalCartao.get().isAtivo()) {
            Pagamento pagamento = new Pagamento();
            pagamento.setCartaoId(optionalCartao.get().getId());
            pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
            pagamento.setValor(pagamentoEntradaDTO.getValor());

            return pagamentoRepository.save(pagamento);
        }else throw new CartaoNotActiveException();
    }

    public Iterable<Pagamento> buscaTodosPagamentosDoCartao(int idCartao){

        Optional<CartaoDTO> optionalCartao = cartaoClient.consultaCartao(idCartao);

        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(idCartao);

        if(pagamentos.isEmpty()) throw new PagamentosNotFoundException();

        return pagamentos;
    }

    public void deletarPagamento(int cartaoId){
        Iterable<Pagamento> pagamentos = buscaTodosPagamentosDoCartao(cartaoId);

        pagamentoRepository.deleteCartaoId(cartaoId);
        //pagamentoRepository.de

//        List<Pagamento> pagamentos = (List<Pagamento>) buscaTodosPagamentosDoCartao(cartaoId);
//
//        if (pagamentos.isEmpty())
//
//        if(pagamentoRepository.findAllByCartaoId(cartaoId)){
//            pagamentoRepository.deleteById(id);
//        }else
//            throw new RuntimeException("O Produto não foi encontrado");
    }

}
