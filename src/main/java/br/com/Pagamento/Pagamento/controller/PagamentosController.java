package br.com.Pagamento.Pagamento.controller;

import br.com.Pagamento.Pagamento.models.DTO.MappingDTO;
import br.com.Pagamento.Pagamento.models.DTO.PagamentoEntradaDTO;
import br.com.Pagamento.Pagamento.models.DTO.PagamentoSaidaDTO;
import br.com.Pagamento.Pagamento.models.Pagamento;
import br.com.Pagamento.Pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
public class PagamentosController {

    @Autowired
    private PagamentoService pagamentoService;

    MappingDTO mappingDTO = new MappingDTO();

    @PostMapping("pagamento")
    public ResponseEntity<PagamentoSaidaDTO> criarPagamento(@RequestBody PagamentoEntradaDTO pagamentoEntradaDTO) {
        PagamentoSaidaDTO pagamentoObejeto = mappingDTO.mappingPagamentoDTO(
                pagamentoService.criarPagamento(pagamentoEntradaDTO));
        return ResponseEntity.status(201).body(pagamentoObejeto);
    }

    @GetMapping("pagamentos/{id_cartao}")
    public ResponseEntity<Iterable<PagamentoSaidaDTO>> buscaTodosPagamentosDoCartao(@PathVariable(name = "id_cartao") int id_cartao) {
        List<PagamentoSaidaDTO> listaPagamentos = new ArrayList<>();
        for (Pagamento pagamento : pagamentoService.buscaTodosPagamentosDoCartao(id_cartao)) {
            listaPagamentos.add(mappingDTO.mappingPagamentoDTO(pagamento));
        }
        return ResponseEntity.status(200).body(listaPagamentos);
    }

    @DeleteMapping("pagamentos/{cartaoId}")
    public void deletarPagamentos(@PathVariable(name = "cartaoId") int cartaoId) {
        pagamentoService.deletarPagamento(cartaoId);
    }


}